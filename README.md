# Products API

> Test application for FCamara

## Build Setup

``` bash
# go to project folder
cd .../products-server/

# change mongodb server address in file src/main/resources/application.properties
spring.data.mongodb.uri=mongodb://localhost:27017/products

# run gradle task (it will download all dependencies and run the api
./gradlew bootRun

````

Now follow products-web project instructions for SPA run