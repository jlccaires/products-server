package com.jlccaires.test.products.repository;

import com.jlccaires.test.products.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRespository extends MongoRepository<Product, String> {

}
