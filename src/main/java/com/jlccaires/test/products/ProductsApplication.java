package com.jlccaires.test.products;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jlccaires.test.products.model.Product;
import com.jlccaires.test.products.repository.ProductRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;

@SpringBootApplication
public class ProductsApplication implements CommandLineRunner {

	@Autowired
	ResourceLoader resourceLoader;
	@Autowired
	ProductRespository productRespository;

	public static void main(String[] args) {
		SpringApplication.run(ProductsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Resource resource = resourceLoader.getResource("classpath:data.json");
		InputStream inputStream = resource.getInputStream();

		productRespository.deleteAll();

		ObjectMapper objectMapper = new ObjectMapper();
		List<Product> products = objectMapper.readValue(inputStream, new TypeReference<List<Product>>() {});
		products.forEach(product -> productRespository.save(product));

	}
}
