package com.jlccaires.test.products.controller;

import com.jlccaires.test.products.model.Product;
import com.jlccaires.test.products.repository.ProductRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@PreAuthorize("isAuthenticated()")
@RestController
@RequestMapping("/products/")
public class ProductController {

    @Autowired
    ProductRespository productRespository;

    @GetMapping("/")
    public Page<Product> list(Pageable pageable) {
       return productRespository.findAll(pageable);
    }
}
