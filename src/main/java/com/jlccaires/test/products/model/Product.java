package com.jlccaires.test.products.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@AllArgsConstructor
@Data
@EqualsAndHashCode(exclude = {"desc", "price"})
public class Product {

    @Id
    String id;
    String desc;
    Float price;


}
